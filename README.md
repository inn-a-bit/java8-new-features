## New Features in Java 8 ##
- ### Lambda expressions
- ### Functional Interfaces
- ### Static & default methods in Interfaces
- ### Optional class
- ### Streams
- ### Date & Time API
###### and more ... 
