package mix_and_match;

interface DefaultDemo
{ 
	default void _default() 
	{ 
		System.out.println("Latin: velle est posse"); 
	} 
} 
interface DefaultDemo2 
{ 
	default void _default() 
	{ 
		System.out.println("English: to be willing is to be able"); 
	} 
} 
/*
 * Declaration below will result in "Duplicate default method name" error
 * ============================================================================
 * public class SameDefaultMethodSignature implements DefaultDemo, DefaultDemo2
 * ============================================================================
 * Duplicate default methods named _default with the parameters () 
 * and () are inherited from the types DefaultDemo2 and DefaultDemo
 * "Duplicate default method name" error will be resolved if the _default method is defined  by the SameDefaultMethodSignature class.
 */
public class SameDefaultMethodSignature implements DefaultDemo, DefaultDemo2
{ 
	public static void main(String args[])
	{ 
		SameDefaultMethodSignature _default = new SameDefaultMethodSignature(); 
		_default._default(); 
	}
	public void _default()
	{
		DefaultDemo.super._default();
	}
}


