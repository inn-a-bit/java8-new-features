package mix_and_match;

import java.util.Arrays;

import java.util.function.Consumer;


public class MethodReference {
	public static void main(String ...str)
	{
		String[] stringArray = { "Lena", "Nata", "Nina", "Inna"};

		// method reference
		Arrays.sort(stringArray, String::compareToIgnoreCase);
		
		// lambda expression 
		Arrays.sort(stringArray, (a, b) -> a.compareToIgnoreCase(b));
		
		System.out.printf("Sorted array[] : %s \n", Arrays.toString(stringArray));	
		
		System.out.println("===========");
		Integer[] array = { 10, 2, 19, 5, 17 };
		  
		// method reference
		Consumer<Integer[]> consumer = Arrays::sort;
	      
		//lambda expression
		//Consumer<int[]> consumer = (a) -> Arrays.sort(a);
	      
		consumer.accept(array);
		Arrays.asList(array).stream().forEach(System.out::println);
	}
}