package mix_and_match;

import java.util.Arrays;
import java.util.List;


public class ForEachOrdered_vs_ForEach {
	public static void main(String ...str)
	{
		String[] stringArray = { "Lena", "Nata", "Nina", "Inna"};
		List<String> array_as_list = Arrays.asList(stringArray);
		
		// For parallel stream pipelines, 'forEach' operation does not guarantee to respect the encounter order of the stream, 
		// as doing so would sacrifice the benefit of parallelism. 
		System.out.println("======forEach======");
		array_as_list.stream().parallel().forEach(System.out::println);
		
		//'forEachOrdered' operation processes the elements one at a time, in encounter order.
		System.out.println("======forEachOrdered======");
		array_as_list.stream().parallel().forEachOrdered(System.out::println);
	}
}