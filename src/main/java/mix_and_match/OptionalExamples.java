package mix_and_match;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import functional_interfaces.pojo.User;
import functional_interfaces.pojo.PhoneNumber;

public class OptionalExamples {

	public static void main(String[] args){

		User user = new User("Lena", 795);

		user.getPhone_number_list().add(new PhoneNumber("12345677", "mobile"));

		Optional<User> _optionalUser = Optional.of(user);

		// Returns an Optional with the specified present non-null value.
		Optional<String> optional = Optional.of("String");
		System.out.println("Optional.of(\"String\").isPresent() <" + optional.isPresent() + ">");

		Optional<User> optionalUser = Optional.of(new User("Lena", 795));

		// filter
		System.out.println("optionalUser.filter(u -> u.getCredit_score() == 795) <"
				+ optionalUser.filter(u -> u.getCredit_score() == 795).get() + ">");

		// ifPresent
		optionalUser.ifPresent(u -> u.setCredit_score(800));
		System.out.println(
				"Optional.of(new User(\"Lena\", 795)).ifPresent() <" + optionalUser.get().getCredit_score() + ">");

		System.out.println(optionalUser.filter(u -> u.getCredit_score() == 795));

		// map
		System.out.println("map() " + _optionalUser.map(u -> {
																return Optional.of(u.getPhone_number_list());
															}).get());

		// Returns an Optional describing the specified value, if non-null,
		// otherwise returns an empty Optional.
		optional = Optional.ofNullable(new String());
		System.out.println("Optional.ofNullable(new String()).isPresent() <" + optional.isPresent() + ">");

		optional = Optional.of("Spring");

		// orElse
		System.out.println(optional.orElse("Winter"));
		optional = Optional.ofNullable(null);
		System.out.println(optional.orElse("orElse()"));

		optional = Optional.ofNullable(null);
		optional.ifPresent(s -> System.out.println(s));
		
		// isPresent
		System.out.println("Optional.ofNullable(null).isPresent() <" + optional.isPresent() + ">");

		// NPE
		// Optional<String> opt_null = Optional.of(null);
		optional.orElseThrow(() -> {
			throw new RuntimeException("Optional is null");
		});
	}

}
