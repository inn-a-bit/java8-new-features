package mix_and_match;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import functional_interfaces.pojo.User;

public class CollectorsGroupBy {
static int i;
	public static void main(String[] args) throws Exception{
        
		User Lena = new User("Lena", 800);
		User Nina = new User("Nina", 795);
		User Nata = new User("Nata", 795);	 
		List<User> users_list = Arrays.asList(Lena, Nina, Nata);
		
		List<User> users = users_list.stream()
											.collect(Collectors.toList()); 
		users.forEach(System.out::println);
    	Map<Integer, List<User>> group_by_score_cltn = 
						    			users_list.stream()
						    			.collect(Collectors.groupingBy(User::getCredit_score));
    	System.out.println("Group by collectiomn size: " +group_by_score_cltn.size() );
    	for(Integer key: group_by_score_cltn.keySet())
		{
    		List<List<User>> list =  Arrays.asList(group_by_score_cltn.get(key));
    		i = 1;
    		list.stream().flatMap(u -> u.stream()).forEach(u -> {System.out.println(i + ". ========>>>" + u); i++;});
		}
	}
}
