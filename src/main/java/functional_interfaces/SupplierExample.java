package functional_interfaces;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import functional_interfaces.pojo.User;
 
public class SupplierExample {
	 // "Supplier" is a functional interface with one method: T get()  with no parameters, and a return type of T
	 // "forEach(Consumer<? super T> action){}"  performs an action for each element of this stream
	
    public static void main(String[] args) {
    	List<String> words = new ArrayList<String>();
    	
        System.out.println("Usus est magister optimus");
        words.add("Practice");
        words.add("is");
        words.add("the best");       
        words.add("teacher"); 
        words.stream().forEach((word)-> print(()-> word, word));
        
        System.out.println("\n\nUsus est magister optimus");        
        print(() -> words.get(0), words.get(0));
        print(() -> words.get(1), words.get(1));
        print(() -> words.get(2), words.get(2));
        print(() -> words.get(3), words.get(3));
        
        System.out.println("\n\n");    
        Supplier<User> user_supplier = () -> new User("Magister", 800);
        System.out.println(user_supplier.get());
    }
   
    private static void print(Supplier<String> supplier, String word) {    	
        System.out.print(supplier.get().replace(word, word + " "));
    }   
}
