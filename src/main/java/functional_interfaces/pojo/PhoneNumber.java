package functional_interfaces.pojo;

import java.util.Comparator;

public class PhoneNumber implements Comparable<PhoneNumber>{
	
	private String phone_number;
	private String type;
	
	public PhoneNumber(String phone_number, String type)
	{		
		this.phone_number = phone_number;
		this.type = type;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public String getPhone_number() {
		return phone_number;
	}

	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}
	@Override
	public String toString(){
		return this.phone_number + " is " + this.type;
	}
	@Override
	public int compareTo(PhoneNumber o)
	{
		return Comparator.comparing((PhoneNumber p)->p.phone_number)
						        						.thenComparing(p->p.type)
						        						.compare(this, o);
	}
	
	@Override
	public boolean equals(Object o)
	{		
		if (o == this) { 
            return true; 
        }
		if (!(o instanceof PhoneNumber)) { 
            return false; 
        } 
		PhoneNumber user = (PhoneNumber) o;
		return this.phone_number.equals(user.phone_number);
	}
	@Override
	public int hashCode()
	{
		return this.phone_number.hashCode();
	}
	
}
