package functional_interfaces.pojo;

import java.util.ArrayList;
import java.util.List;

public 	class User implements Comparable<User>{
	String name;
	int credit_score;
	List<PhoneNumber> phone_number_list = new ArrayList<>();
	
	public User(String name, int credit_score)
	{
		this.name = name;
		this.credit_score = credit_score;
	}
	public User(String name, int credit_score, 
			List<PhoneNumber> phone_list)
	{
		this.name = name;
		this.credit_score = credit_score;
		this.phone_number_list = phone_list;
	}
	public List<PhoneNumber> getPhone_number_list() {
		return phone_number_list;
	}
	public void setPhone_number_list(List<PhoneNumber> phone_number_list) {
		this.phone_number_list = phone_number_list;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCredit_score() {
		return credit_score;
	}
	public void setCredit_score(int credit_score) {
		this.credit_score = credit_score;
	}
	
	@Override
	public boolean equals(Object o)
	{		
		if (o == this) { 
            return true; 
        }
		if (!(o instanceof User)) { 
            return false; 
        } 
		User user = (User) o;
		return this.name.equals(user.name)  && this.credit_score == user.credit_score;
	}
	@Override
	public int hashCode()
	{
		return this.credit_score * 31 + this.name.hashCode();
	}
	@Override 
	public int compareTo(User user)
	{
		int compare_name_int =  this.name.compareTo(user.name);	
		return compare_name_int == 0 ? 
				this.credit_score > user.credit_score ? 1 : 
				this.credit_score < user.credit_score ? -1 : 
				0 :compare_name_int ;
	}
	@Override
	public String toString(){
		return this.name + " scored " + this.credit_score;
	}
}
