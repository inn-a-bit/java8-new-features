package functional_interfaces;


import java.util.ArrayList; 
import java.util.List; 
import java.util.function.Consumer; 
  
public class ConsumerExample {
    public static void main(String args[]) 
    { 
    	//Consumer is a functional interface and can  be used as the assignment target 
    	//for a lambda expression.
    	//Represents an operation that accepts a single input argument and returns no result.
    	//has two methods: accept, andThen
    	//abstract void  accept(T t)
    	//default Consumer<T> andThen(Consumer after)
 
    	
  //  	Consumer<String> consumerString=s->System.out.println(s);
    	Consumer<String> consumer_string = System.out::println; // via method reference
    	consumer_string.accept("Viva voice");
    	    	
    	Consumer<String> verbi_gratia_x = x -> System.out.println(x.toLowerCase());
    	Consumer<String> verbi_gratia_y = y -> System.out.println(y.toUpperCase());	    
    	Consumer<String> andThen = verbi_gratia_x.andThen(verbi_gratia_y);    
    	andThen.accept("Venturis Ventis");
    	
        Consumer<List<Integer>> verbi_gratia_3 = list -> { 
												            for (int i = 0; i < list.size(); i++)
												            {
												                list.set(i, 2 * list.get(i));
												            }										           
											        	}; 
  

        Consumer<List<Integer>> verbi_gratia_33 = list -> list.stream().forEach(System.out::println); 
  
        List<Integer> list = new ArrayList<Integer>(); 
        list.add(2); 
        list.add(1); 
        list.add(3); 
  
        verbi_gratia_3.andThen(verbi_gratia_33).accept(list);  
        
    } 
} 