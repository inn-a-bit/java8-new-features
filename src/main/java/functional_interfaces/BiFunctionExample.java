package functional_interfaces;

import java.util.function.BiFunction;
import java.util.function.Function;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class BiFunctionExample {

	// BiFunctional<T,U,R> accepts 2 arguments and produces a result(BiFunction is two-arity specialization of Function)
	// abstract "R apply(T t, U u);"
	// default <V> BiFunction<T,U,V> andThen(Function after){};

	public static void main(String... args) {

		BiFunction<Integer, Integer, Integer> return_integer = (x, y) -> x + y;
		print(return_integer, 10, 71);
		
		BiFunction<Integer, Integer, String> return_string = (x, y) -> 
							"(" + x + " + " + y + ") -> <" + Character.toString((char) (x + y)) + ">";
		print(return_string, 10, 71);

		Function<Integer, String> for_andThen = (x) -> "Square root of " + x + " is "
				+ new Integer((int) Math.sqrt(x)).toString();
		print(for_andThen, 81);

		BiFunction biFunction = return_integer.andThen(for_andThen);
		print(biFunction, 10, 71);
	}

	private static void print(BiFunction function, int x, int y) {
		System.out.println(function.apply(x, y));
	}

	private static void print(Function function, int x) {
		System.out.println(function.apply(x));
	}
}
