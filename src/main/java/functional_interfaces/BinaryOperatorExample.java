package functional_interfaces;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.BinaryOperator;

import functional_interfaces.pojo.User;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class BinaryOperatorExample {

	public static void main(String... args) {
		// BinaryOperator<T,T,T> extends BiFunction.
		// Represents an operation upon two operands of the same type, produces the result of the same type as operands.
		// static <T> BinaryOperator<T> maxBy(Comparator comparator){} returns greater of two elements
		// static <T> BinaryOperator<T> minBy(Comparator comparator){} returns lesser of two elements

		BinaryOperator<Integer> maxBy_int = BinaryOperator.maxBy((a, b) -> (a > b) ? 1 : ((a == b) ? 0 : -1));
		print(maxBy_int, 100, 11);

		BinaryOperator<Integer> minBy_int = BinaryOperator.minBy((a, b) -> (a > b) ? 1 : ((a == b) ? 0 : -1));
		print(minBy_int, 100, 11);

		BinaryOperator<String> minBy_str = BinaryOperator
				.minBy((a, b) -> (a.length() > b.length()) ? 1 : ((a.length() == b.length()) ? 0 : -1));

		BinaryOperator<String> maxBy_str = BinaryOperator
				.maxBy((a, b) -> (a.length() > b.length()) ? 1 : ((a.length() == b.length()) ? 0 : -1));

		print(maxBy_str, "string...", "string");
		print(minBy_str, "string...", "string");

		Comparator<Integer> comparator = Integer::compareTo;
		// Comparator<Integer> comparator = (a, b) -> (a.compareTo(b));
		BinaryOperator<Integer> maxBy_int_ = BinaryOperator.maxBy(comparator);
		BinaryOperator<Integer> minBy_int_ = BinaryOperator.maxBy(comparator);
		print(maxBy_int_, 100, 11);
		print(minBy_int_, 100, 11);

		List<User> user_list = Arrays.asList(new User("Lena", 800), new User("Nata", 770), new User("Nina", 780));

		// Comparator<User> comparing =
		// Comparator.comparing(User::getCredit_score); // via method reference

		Comparator<User> comparing = Comparator.comparing((User u) -> u.getCredit_score());

		BinaryOperator<User> maxByUser = BinaryOperator.maxBy(comparing);
		User user_max = user_list.stream().reduce(new User("", 1), maxByUser);
		System.out.println(user_max);

		BinaryOperator<User> minByUser = BinaryOperator.minBy(comparing);
		User user_min = user_list.stream().reduce(new User("", 800), minByUser);
		System.out.println(user_min);
	}

	private static void print(BinaryOperator function, String x, String y) {
		System.out.println(function.apply(x, y));
	}

	private static void print(BinaryOperator function, int x, int y) {
		System.out.println(function.apply(x, y));
	}
}
