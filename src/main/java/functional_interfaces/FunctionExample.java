package functional_interfaces;

import java.util.function.Function;
import functional_interfaces.pojo.User;

public class FunctionExample {

	public static void main(String[] args) {

		// Function represents a function that accepts one argument and returns the result.
		// abstract method "R apply(T t){}"
		// takes an argument and returns the result

		Function<User, String> apply_function = (User u) -> u.getName() + " with a high score of "
				+ u.getCredit_score();
		User u = new User("Function User", 750);
		print(apply_function, u);

		// default function "Function<T,V> andThen(Function<T, V> after){}"
		// executes the caller first and the parameter last

		Function<String, String> for_andThen_function = (String uu) -> uu.toUpperCase();
		Function<User, String> andThen_function = apply_function.andThen(for_andThen_function);
		print(andThen_function, u);

		// default function "Function compose(Function before){}"
		// 'compose' executes the parameter first and the caller last
		// 'apply_function' will be executed first
		// input is 'User', output 'String';
		// 'for_compose_funtion' will be executed last, with input 'String' and output 'Integer'
		// 'Function<User, Integer>' => 'User' is input to 'apply_function',
		// 'Integer' is the output from 'for_compose_function'

		Function<String, Integer> for_compose_function = (String uu) -> uu.length();
		Function<User, Integer> compose_function = for_compose_function.compose(apply_function);
		print(compose_function, u, "compose");

	}

	private static void print(Function<User, String> function, User u) {
		System.out.println(function.apply(u));
	}

	private static void print(Function<User, Integer> function, User u, String str) {
		System.out.println("==   " + str + " returns   == >> " + function.apply(u));
	}

}
