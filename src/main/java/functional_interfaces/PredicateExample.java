package functional_interfaces;

import java.util.function.Predicate;

public class PredicateExample {

	public static void main(String[] args) {
		
		// abstract method "boolean test(T t);"
		
		Predicate<String> verbi_gratia_x = (x) -> x.length() == 17;
		Predicate<String> verbi_gratia_y = (y) -> y.equals("Scientia et labor");
		Predicate<String> verbi_gratia_z = (z) -> z.equals("Knowledge and wisdom");
		Predicate<String> verbi_gratia_w = (w) -> w.equals("Scientia et sapientia");
		
		print(verbi_gratia_x, "Scientia et labor", "verbi_gratia_x");
		
		// default method "default Predicate<T> and(Predicate<? super T> other)"
		Predicate<String> and_predicate = verbi_gratia_x.and(verbi_gratia_y);
		print(and_predicate, "Scientia et labor", "and_predicate"); 
		
		// static method "static <T>Predicate<T> isEqual(Object targetRef){}" 
		// returns a predicate that tests if two arguments are equal.
		
		Predicate<String> isEqual_predicate = Predicate.isEqual("scientia et sapientia");
		print(isEqual_predicate,  "scientia et sapientia", "isEqual_predicate");
		
		// default method "default Predicate<T> negate(){}" 
		// returns a predicate that represents the logical negation of this predicate.
		Predicate<String> negate_predicate = verbi_gratia_y.negate();
		print(negate_predicate, "knowledge and work" , "negate_predicate");
		
		// default method "default Predicate<T> or<Predicate<? super T> other" 
		// returns Predicate from logical OR.
		Predicate<String> or_predicate = verbi_gratia_w.or(verbi_gratia_z);
		print(or_predicate, "Scientia et sapientia" , "or_predicate");
		print(or_predicate, "Knowledge and wisdom" , "or_predicate");
	}
	static void print(Predicate<String> predicate, String phrase, String predicate_str)
	{
		System.out.println("==== " + predicate_str + " ====");
		System.out.println("\t" + predicate.test(phrase));
	}
}
