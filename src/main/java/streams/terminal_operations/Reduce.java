package streams.terminal_operations;

import java.util.Arrays;
import java.util.List;

import functional_interfaces.pojo.User;

public class Reduce {

public static void main(String[] args) {
		
		User Lena = new User("Lena", 800);
		User Nina = new User("Nina", 700);
		User Nata = new User("Nata", 750);
		
		List<User> customers = Arrays.asList(Lena, Nata, Nina);
		
		int sum = customers.stream()
								.map( u -> u.getCredit_score())
								.reduce(0, (a, b) -> {System.out.println(a + "+" + b); return a + b;});
		System.out.println(sum);
		
	}
}