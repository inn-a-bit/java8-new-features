package streams.terminal_operations;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import functional_interfaces.pojo.User;

public class Min {

public static void main(String[] args) {
		
		User Lena = new User("Lena", 800);
		User Nina = new User("Nina", 795);
		User Nata = new User("Nata", 787);
		
		List<User> customers = Arrays.asList(Lena, Nata, Nina);
		
		Comparator<User> comparing = Comparator.comparing((User u) -> u.getCredit_score());
		
		Optional<User> highScoredUser = customers.stream()
								.max(comparing);
		System.out.println(highScoredUser.get());
		
	}
}