package streams.terminal_operations;

import java.util.Arrays;
import java.util.List;

import functional_interfaces.pojo.User;

public class Count {

	public static void main(String[] args) {
		User Lena = new User("Lena", 800);
		User Nina = new User("Nina", 795);
		User Nata = new User("Nata", 795);
		 
		// long count()
		// Returns count of elements in this stream
		List<User> users = Arrays.asList(Lena, Nina, Nata);
		long count = users.stream()
							.count();
		System.out.println(count);

	}

}
