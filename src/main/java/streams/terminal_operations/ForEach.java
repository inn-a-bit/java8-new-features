package streams.terminal_operations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Stream;

import functional_interfaces.pojo.User;

public class ForEach {

public static void main(String[] args) {
		
		List<String> source = new ArrayList<>(Arrays.asList("Viam", "supervadet", "vadens"));
		List<Integer> target = new ArrayList<>();		
		
		
		// map(Function), forEach(Consumer)
		// <R> Stream<R> map(Function<? super T, ? extends R> mapper);
		// void forEach(Consumer<? super T> action);
		
		Stream<String> stream = source.stream();
		Stream<Integer> stream_map = stream.map(s -> { 
														System.out.print( s + " " );
														return s.length();
													});
		stream_map.forEach( i -> target.add(i));
		
		// via method chaining
		// stream.map(s -> s.length()).forEach( s -> target.add(s));
		// ---------------------------------------------------------
		// with method reference
		// source.stream().map(String::length).forEach(target::add);
		
		
		stream.close();
		stream_map.close();
		
		for(Integer each: target)
			System.out.print("\n" + each);	
		
		User Lena = new User("Lena", 800);
		User Nina = new User("Nina", 795);
		User Nata = new User("Nata", 795);
		
		List<User> users = Arrays.asList(Lena, Nina, Nata);
		Set<String> user_name_list = new TreeSet<>();
		users.stream().map(u -> u.getName()).forEach(s ->  user_name_list.add(s));
		user_name_list.forEach(u -> System.out.print("\n" + u));
	}
}
