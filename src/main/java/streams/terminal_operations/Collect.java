package streams.terminal_operations;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import functional_interfaces.pojo.User;

public class Collect {

	public static void main(String[] args) throws Exception{
		
		// <R,A> R collect(Collector<? super T, A, R> collector)
		List<String> list = Arrays.asList("9", "A", "Z", "1", "B", "Y", "4", "a", "c");
        Set<String> hashSet = list.stream()
        								.collect(Collectors.toSet());   // this will return a HashSet    
        hashSet.forEach(System.out::println);
        
        Set<String> treeSet = list.stream()
										.collect(TreeSet::new, TreeSet::add, TreeSet::addAll);
        treeSet.forEach(System.out::println);
        
		User Lena = new User("Lena", 800);
		User Nina = new User("Nina", 795);
		User Nata = new User("Nata", 799);	 
		List<User> users_list = Arrays.asList(Lena, Nina, Nata);
		
		List<User> sortedUserList = users_list.stream()
											.sorted()
											.collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
											//.collect(Collectors.toList()); // same as the above
    	sortedUserList.forEach(System.out::println);
    	
		Set<User> userTreeSet = users_list.stream()
										.collect(TreeSet::new, TreeSet::add, TreeSet::addAll);  
		userTreeSet.forEach(System.out::println);
          
		// create stream from a file
		Stream<String> rowsFromFile = Files.lines(Paths.get("/path/to/data.txt"));

		Map<String, Integer> map = rowsFromFile
									.map(x -> x.split(","))
									.filter(y-> y.length == 3)
									.filter(x -> Integer.parseInt(x[2]) >40)
									.collect(Collectors.toMap(z -> z[0], z -> Integer.parseInt(z[1])));
		rowsFromFile.close();	
		for(String key: map.keySet())
		{
			System.out.println(key + " : " + map.get(key));
		}
		Integer credit_score_sum = users_list.stream()
										.map(u -> u.getCredit_score())
										.collect(Collectors.summingInt(Integer::intValue));
		System.out.println(credit_score_sum);

	}

}
