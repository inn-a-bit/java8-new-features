package streams.terminal_operations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import functional_interfaces.pojo.User;

public class FindFirst {

public static void main(String[] args) {
		
		User Lena = new User("Lena", 800);
		User Nina = new User("Nina", 795);
		User Nata = new User("Nata", 787);
		
		List<User> customers = Arrays.asList(Lena, Nata, Nina);
		
		Optional<User> highScoredUser = customers.stream()
					.filter(c -> c.getCredit_score() > 790)
					.findFirst();
		System.out.println(highScoredUser.isPresent());
		
		customers.stream()
					.filter(c -> c.getCredit_score() > 790)
					.findFirst()
					.ifPresent(System.out::println);
					
		Optional<User> veryHighScoredUser = customers.stream()
				  									.filter(c -> c.getCredit_score() > 800)
				  									.findFirst();		
		System.out.println(veryHighScoredUser.isPresent());
		
		customers.stream()
					.filter(c -> c.getCredit_score() > 800)
					.findFirst()
					.ifPresent(System.out::println);
		
	}
}