package streams.terminal_operations;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

import functional_interfaces.pojo.User;

public class NoneMatch {

public static void main(String[] args) {
		
		User Lena = new User("Lena", 800);
		User Nina = new User("Nina", 795);
		User Nata = new User("Nata", 787);
		
		List<User> customers = Arrays.asList(Lena, Nata, Nina);
		
		Predicate<User> predicate = (User u) -> u.getCredit_score() == 795;
		
		boolean bol = customers.stream()
								.noneMatch(predicate);
		System.out.println(bol);
		
	}
}