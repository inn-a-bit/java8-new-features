package streams.terminal_operations;

import java.util.Arrays;
import java.util.List;

import functional_interfaces.pojo.User;

public class AnyMatch {

	public static void main(String[] args) {
		User Lena = new User("Lena", 800);
		User Nina = new User("Nina", 795);
		User Nata = new User("Nata", 799);
		 
		// boolean anyMatch(Predicate<? super T> predicate)
		// Returns true if any of this stream match the provided predicate.
		List<User> users = Arrays.asList(Lena, Nina, Nata);
		boolean anyMatch = users.stream()
							.anyMatch((User u) -> u.getName().startsWith("N") && u.getCredit_score() == 795);
		System.out.println(anyMatch);
		
		boolean anyMatch_false = users.stream()
							.anyMatch((User u) -> u.getName().endsWith("ta") &&  u.getCredit_score() == 795);
		System.out.println(anyMatch_false);

	}

}
