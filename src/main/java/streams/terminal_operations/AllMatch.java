package streams.terminal_operations;

import java.util.Arrays;
import java.util.List;

import functional_interfaces.pojo.User;

public class AllMatch {

	public static void main(String[] args) {
		User Lena = new User("Lena", 800);
		User Nina = new User("Nina", 795);
		User Nata = new User("Nata", 795);
		
		// boolean allMatch(Predicate<? super T> predicate
		// returns true, if all elements of the stream match the provided predicate, false otherwise 
		List<User> users = Arrays.asList(Lena, Nina, Nata);
		boolean allMatch = users.stream()
							.allMatch((User u) -> u.getName().length() == 4);
		System.out.println(allMatch);
				
		boolean allMatch_false = users.stream()
							.allMatch((User u) -> u.getCredit_score() == 795);
		System.out.println(allMatch_false);

	}

}
