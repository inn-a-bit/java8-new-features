package streams.intermediate_operations;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import functional_interfaces.pojo.PhoneNumber;
import functional_interfaces.pojo.User;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;

public class FlatMap {

	public static void main(String[] args) {
	    List<List<String>> namesNested = Arrays.asList( 
	  	      Arrays.asList("Lena", "Talent"), 
	  	      Arrays.asList("Nata", "Leada"), 
	  	      Arrays.asList("Nina", "Wisdom"));

	  	    List<String> namesFlatStream = namesNested.stream()
	  	    								.flatMap(Collection::stream)
	  	    								.collect(toList());
	  	    
	  	    for(String name: namesFlatStream)
	  	    {
	  	    	System.out.println(name);
	  	    }
	  	    
		List<PhoneNumber> phone_list = new ArrayList<PhoneNumber>();
		PhoneNumber landline = new PhoneNumber("1 847-222-3333", "landline");
		PhoneNumber mobile = new PhoneNumber("1 847-222-3334", "mobile");
		phone_list = Arrays.asList(landline, mobile);
		User Lena = new User("Lena", 800, phone_list);
		User Nina = new User("Nina", 790, new ArrayList<PhoneNumber>());
		User Nata = new User("Nata", 790, new ArrayList<PhoneNumber>());
		
		 
		List<User> users = Arrays.asList(Lena, Nina, Nata);
		users.stream()
				.flatMap(user -> user.getPhone_number_list().stream())
				.forEach(System.out::println);
	}
	
}
