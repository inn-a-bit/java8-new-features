package streams.intermediate_operations;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class Concat {

	public static void main(String[] args) {
	
		List<String> stream_1 = Arrays.asList("'Usus", "est", "magister", "optimus'");
		List<String> stream_2 = Arrays.asList( " == ", "'Practice", "is the best teacher'");
		Stream.concat(stream_1.stream(), stream_2.stream()).forEach(e -> System.out.print(e + " "));		
	}
}
