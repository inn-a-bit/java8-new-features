package streams.intermediate_operations;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import functional_interfaces.pojo.User;

public class Filter {

public static void main(String[] args) {
		
		User Lena = new User("Lena", 800);
		User Nina = new User("Nina", 795);
		User Nata = new User("Nata", 787);
		 
		List<User> customers = Arrays.asList(Lena, Nina, Nata);

		// Stream<T> filter(Predicate<? super T> predicate), 
		// <R, A> R collect(Collector<? super T, A, R> collector), 
		// <R> Stream<R> map(Function<? super T, ? extends R> mapper);
		// Predicate -->> boolean test(T t);
		// Function -->> R apply(T t);
		
		List<User> highScoredUser = customers.stream()
				  									.filter(c -> c.getCredit_score() > 790)
				  									.collect(Collectors.toList());
		
		List<User> highScoredUserN = customers.stream()
														  .filter(c -> c.getCredit_score() >= 790 && c.getName().startsWith("N"))
														  .collect(Collectors.toList());
		List<String> customerNameCltn = customers.stream()
														.map(User::getName)
														.collect(Collectors.toList());
		List<String> filterUserNameCltn = customers.stream()
														.map(User::getName)
														.filter(c -> c.equals("Nina") || c.endsWith("ta"))
														.collect(Collectors.toList());
		Set<String> userNameSorted = customers.stream()
														.map(User::getName)
														.collect(Collectors.toCollection(TreeSet::new));

		print(highScoredUser);
		print(highScoredUserN);
		print(customerNameCltn);
		print(filterUserNameCltn);
		print(userNameSorted);
		
	}
	private static <T>  void print(Collection<? extends T> user_collection)
	{	
		for(T each: user_collection)
		{
			System.out.println(each);
		}
		System.out.println("===================");
	}
}