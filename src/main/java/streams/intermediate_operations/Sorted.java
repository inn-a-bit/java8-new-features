package streams.intermediate_operations;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import functional_interfaces.pojo.User;

public class Sorted {

	public static void main(String[] args) throws Exception{
		
		List<String> list = Arrays.asList("9", "A", "Z", "1", "B", "Y", "4", "a", "c");
        List<String> sortedList = list.stream()
        								.sorted()
        								.collect(Collectors.toList());       
        sortedList.forEach(System.out::println);
        
		User Lena = new User("Lena", 800);
		User Nina = new User("Nina", 795);
		User Nata = new User("Nata", 799);	 
		List<User> users_list = Arrays.asList(Lena, Nina, Nata);
		
		/* 
			List<User> sortedUserList = users_list.stream().sorted().collect(Collectors.toList()); 
			-------------------------------------------------------------------------------------- 
			List<String> sortedList = list.stream()
            .sorted((o1,o2)-> o1.compareTo(o2))
            .collect(Collectors.toList());
        */
	
        List<User> sortedUserList = users_list.stream()
            .sorted(Comparator.naturalOrder())
            .collect(Collectors.toList());
        
    	sortedUserList.forEach(System.out::println);
 	    System.out.println("---- Sort by score and name ----\n");
        ToIntFunction<User> function = (u) -> ((User) u).getCredit_score();            
        List<User> sortedList2 = users_list.stream()
						           // .sorted(Comparator.comparingInt(User::getCredit_score)) via method reference
						            .sorted(Comparator.comparingInt(function)
						            					.thenComparing(User::getName))
						            .collect(Collectors.toList());	 
        sortedList2.forEach(System.out::println);
        
        System.out.println("\n---- Sort by name ---\n");
        sortedList2 = users_list.stream()
				 	           	.sorted(Comparator.comparing(User::getName)) 
				 	            .collect(Collectors.toList());
        sortedList.forEach(System.out::println);
	}

}
