package streams.intermediate_operations;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class _Map {

	public static void main(String[] args) {
		
		List<String> myList = Stream.of("Lena", "Nata", "Nina")
				  .map(String::toUpperCase)
				  .collect(Collectors.toList());
		System.out.println(myList);
		
		Map<String, String> sisters = new HashMap<>();
		sisters.put("Lena", "Talented");
		sisters.put("Nata", "Enthusiastic");
		sisters.put("Nina", "Wise");
				
		Optional<String> trait = sisters.entrySet().stream()
						  .filter(e -> "Wise".equals(e.getValue()))
						  .map(Map.Entry::getKey)
						  .findFirst();
		System.out.println(trait.get());
				
		Optional<String> no_trait = sisters.entrySet().stream()
						  .filter(e -> " ".equals(e.getValue()))
						  .map(Map.Entry::getKey)
						  .findFirst();
		System.out.println(no_trait.isPresent());				
	}

}
